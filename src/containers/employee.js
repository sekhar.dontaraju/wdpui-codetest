var React = require('react');
var Avatar= require('../components/avatar.js');
var Labeltext = require('../components/label.js');
var Freetext = require('../components/freetext.js');
var Modal = require('../components/employeeModal.js');
var dataSet = require('../store/employeeInfo.json');


class Employee extends React.Component {

    constructor(props) {
        super(props);

        this.state = { isOpen: false };
        this.toggleModal = this.toggleModal.bind(this);
        this.citem;
    }

    render() {
        return (
            <div className="container">
                <div className="row top-padding">
                    <h4>Our Employees</h4> <hr />
                </div>
                <div className="text-center div-padding">
                    {this.createItems(dataSet.employees)}
                </div>
            </div>
        );
    }

    toggleModal(i, event) {
        this.setState({
            isOpen: !this.state.isOpen,
        });
        this.citem = i;
    }

    // this.toggleDisplay = function(i, event){
    //       this.setState({
    //         display: !this.state.display
    //       });
    //     }.bind( this );

    //     this.showModal = function(){
    //       this.setState( { modal: true } );
    //     }.bind( this );

    //     this.hideModal = function(){
    //       this.setState( { modal: false } );
    //     }.bind( this );

    // componentDidUpdate( prevProps, prevState ){
    //   // if toggled to display
    //   if( !prevState.display && this.state.display ){
    //     if( !this.state.modal ){
    //       setTimeout( this.showModal, 3000 ); //delay 3 seconds
    //     }
    //   }

    //   // if toggled to not display
    //   else if( prevState.display && !this.state.display ){
    //     if( this.state.modal ){
    //       this.hideModal(); //assuming you don't want to delay hide
    //     }
    //   }
    // }


    createItems(items) {
        return items.map(function (item, i) {
            return (<div className="col-md-4 col-sm-6 col-lg-4" key={i} onClick={this.toggleModal.bind(this, i)}>
                <div className="col-md-11 text-left div-border card-padding">
                    <div className="col-md-6 card-padding" >
                        <Avatar source={item.avatar} />
                    </div>
                    <div className="col-md-6 div-padding">
                        <Labeltext source={item.firstName + ' ' + item.lastName} />
                        <Freetext source={item.shortbio} />
                    </div>
                </div>
                <Modal show={this.state.isOpen}
                    onClose={this.toggleModal}
                    thisitem={this.citem}>
                </Modal>
           </div>)
        }, this)
    }

    
};

module.exports = Employee;