
var React = require('react');
var ReactDOM = require('react-dom');
var Employee = require('./containers/employee.js');
var Header = require('./components/header.js');

ReactDOM.render(<Header />, document.getElementById('header-application'));     
ReactDOM.render(<Employee />, document.getElementById('data-react-application'));          