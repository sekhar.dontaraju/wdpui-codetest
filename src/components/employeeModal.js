var React = require('react');
var Avatar = require('./image.js');
var Labeltext = require('./label.js');
var Normaltext = require('./freetext.js');
var data = require('../store/employeeInfo.json');

class EmployeeModel extends React.Component {
    render() {
        var node = this.props.thisitem;

    if(!this.props.show) {
      return null;
    }

    return (
        <div className="backdrop-style" onClick={this.props.onClose} >
            <div className="text-right modal-style-close">
                <img src="./images/close.png" alt="close" width="24px" onClick={this.props.onClose} />
            </div>
            <div className="modal-style">
                <div className="col-sm-12 text-left div-border-modal">
                    <div className="col-sm-4 div-padding">
                        <Avatar source={data.employees[node].avatar} />
                        <Labeltext source={data.employees[node].jobTitle} /><br />
                        <Labeltext source={data.employees[node].age} /><br />
                        <Labeltext source={data.employees[node].dateJoined} /><br />
                    </div>
                    <div className="col-sm-7">
                        <Labeltext source={data.employees[node].firstName + ' ' + data.employees[node].lastName} /><br />
                        <hr />
                        <Normaltext source={data.employees[node].bio} />
                        <br /><br />
                    </div>
                </div>
        </div>
      </div>
    );
  }
}

module.exports = EmployeeModel; 