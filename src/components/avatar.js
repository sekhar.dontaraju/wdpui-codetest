var React = require('react');

let Avatar = function statelessFunctionComponentClass(props) {
  let source = './images/' + props.source;

  let style = {
    width: '150px',
    margin: '0px',
    border: 'solid',
    width:'100%'
  };

  return (
    <img src={source} style={style} />
  );
};

module.exports = Avatar;