﻿var React = require('react');

let Freetext = function statelessFunctionComponentClass(props) {
    let source = props.source;

    let style = {
        margin: '10px 0px 0px 5px',
        fontWeight: '400',
        display: 'inline-block'
    };

    return (
        <label htmlFor={source} style={style}>{source}</label>
    );
};

module.exports = Freetext;