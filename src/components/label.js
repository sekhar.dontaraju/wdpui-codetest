﻿var React = require('react');

let Labeltext = function statelessFunctionComponentClass(props) {
    let source = props.source;

    let style = {
        margin: '10px 0px 0px 5px',
        fontWeight: 'bold',
        display: 'inline-block'
    };

    return (
        <label htmlFor={source} style={style}>{source}</label>
    );
};

module.exports = Labeltext;

