﻿var React = require('react');
var data = require('../store/companyInfo.json');

//var Employee = React.createClass({
class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <div>
                            <h2>{data.companyInfo.companyName}</h2>
                        </div>
                        <div>
                            <div className="col-sm-10"><h5>{data.companyInfo.companyMotto}</h5></div>
                            <div className="col-sm-2 text-left">Since {data.companyInfo.companyEst}</div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        );
    }
};

module.exports = Header;